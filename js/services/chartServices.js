function highcharts(population){
	$('.graph').highcharts({
		title: {text: 'Problación por continentes.'},
        xAxis: {categories: ['Problación']},
        yAxis: {title:'Problación'},
        series: [
        	{type: 'column', name: [population[0].Continent], data: [population[0].total]},
        	{type: 'column', name: [population[1].Continent], data: [population[1].total]},
        	{type: 'column', name: [population[2].Continent], data: [population[2].total]},
        	{type: 'column', name: [population[3].Continent], data: [population[3].total]},
        	{type: 'column', name: [population[4].Continent], data: [population[4].total]},
        	{type: 'column', name: [population[5].Continent], data: [population[5].total]},
        	{type: 'column', name: [population[6].Continent], data: [population[6].total]},
                 
        ]
	});
}

function getContinentName(population){
	var ContinentName = [];

	for(var i = 0; i < population.length; i++){
		ContinentName.push(population[i].Continent);
	}

	return ContinentName;
}