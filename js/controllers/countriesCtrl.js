angular.module('App', [])
	   .controller('countriesCtrl', function($scope, $http) {
	   		$http.get('http://localhost:1337/api/v1/countrys')
	   		.success(function(data) {
	   			//console.log(data);
	   			$scope.countries = data;
	   		})
	   		.error(function(err) {
	   			console.log(err);
	   		})
	   })
	   .controller('GraphCtrl', function($scope, $http) {
	   		$http.get('http://localhost:1337/api/v1/continent_population')
	   		.success(function(data) {
	   			$scope.population = data;
	   			highcharts($scope.population);
	   		})
	   		.error(function(err) {

	   		})
	   })
